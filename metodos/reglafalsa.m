t = cputime();      
      clc;     
      disp('METODO DE REGLA FALSA')
      fx = funcion
      xl = aproximacion1
      xu = aproximacion2
      es = tolerancia
      imax = 10
      %obtenemos el valor evaluado en ese punto. 
      f = inline(fx); 
      ezplot(f);
      fl = f(xl);
      fu = f(xu);
      %Inicializamos el numero de iteraciones
      iter = 0;
      %Variable temporal de iu
      iu = 0;
      %Variable temporal de il
      il = 0;
      xr = 100;
      d = 0;
      while( d==0 ) % en este paso se va reduciendo el intervalo hasta encontar la raiz
          xrold = xr;
          xr = xu-((fu*(xl-xu))/(fl-fu));
          fr = f(xr);
          iter = iter+1; %metodo para determinar el numero maximo de iteraciones
          if( xr~=0 )
              ea = abs(((xr-xrold)/xr) * 100);
          end
          test = fl*fr;
          if(test<0)
              xu = xr;
              fu = f(xu);
              iu = 0;
              il = il+1;
              if( il>=2 )
                  fl=fl/2;
              end
          else
              if(test>0)
                  xl=xr;
                  fl=f(xl);
                  il=0;
                  iu=iu+1;
                    if(iu>=2)
                        fu=fu/2;
                    end
              else
                  ea=0;
              end
              if((ea<es)||(iter>=imax))
                  break;
              end
          end
      end
      %Imprimimos por pantalla:
      printf(' * * * El valor de x es: * * * ')
      disp(xr);          
      printf(' * * * Tiempo en que tarda en ejecutarse: %f segundos * * * \n', cputime-t);