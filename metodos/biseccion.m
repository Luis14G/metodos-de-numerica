t = cputime();      
      clc;
      disp('METODO BISECCION')
      %Obtiene los datos 
      i=intervalo
      fx=funcion
      to=tolerancia
      
      
      %Se define la funcion 
      f=inline(fx) 
      ezplot(f);
      a=I(1);
      b=I(2);
      
      %metodo para determina el numero maximo de iteraciones
      imax=log2(abs((b-a)/to))+1 

      for i=1:imax
        m=(a+b)/2;
        %verifica si hay cambio de signo
        if f(a)*f(m)<0 
          %Remplaza el nuevo extremo b del intervalo por el punto medio
          b=m; 
        else
          %Si la condicion anterior no se cumple remplaza el extremo a del intervalo por el punto medio
          a=m;  
        end
        x(i)=m; 
        error(i)=abs(a-b);
        
      end
      %imprimimos los resultados por pantalla:
      disp(' * * * El valor de la raiz es: * * * '),disp(m)
      disp(' * * * Cantidad de Iteraciones es: * * * '),disp(i)
      disp('Los valores de x para cada iteracion fueron:')
      disp(x')
      disp('Los valores del error para cada iteracion fueron:')
      disp(error')
      printf(' * * * Tiempo en que tarda en ejecutarse: %f segundos * * * \n', cputime-t);