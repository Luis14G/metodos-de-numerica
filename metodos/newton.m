t = cputime();      
        clc;
        disp("Metodo de Newton")
        %solicitamos la funcion y la guardamos en la variable
        fun =funcion
        %Declaramos una variable simbolica
        syms x
        %Transformamos la funcion string en una funcion de octave
        f = inline(fun);
        %Graficamos la funcion
        ezplot(f);
        %Derivamos la funcion
        derivada = diff(fun,x);
        %solicitamos la toleracia
        tol = tolerancia
        %Declaramos un error alto
        error = 50;
        %solicitamos el valor inicial
        x0 = valorinicial
        %Variable temporal de x0
        x1 = 0;
        %Inicializamos el numero de iteraciones
        iter = 0;
        %Imprimimos por pantalla 
        %iteracion = numero de la iteracion
        %xi = valor de x
        %error = error obtenido
        disp('    iteracion      xi       error')
        %Inicio del cicl0
        while (error>tol)
          %Imprimir la iteracion, xi y error correspondiente al ciclo
          fprintf('\t%i\t%3.5f\t%f\n', iter,  x0, error);
          %Paso numero 3 del metodo de newton raphson
          x1 = x0 - (f(x0)/(double(subs(derivada,x0))));
          %Calculo del error
          error= abs(x1 - x0);
          %Almacenamos la variable temporal x1 en x0
          x0 = x1;
          %Aumentamos el numero de la iteracion
          iter = iter + 1;
        end
        %Imprimimos por pantalla:
        disp(' * * * El valor de la raiz es:'), disp(x0);
        printf(' * * * Tiempo en que tarda en ejecutarse: %f segundos * * * \n', cputime-t);