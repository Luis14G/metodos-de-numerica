t = cputime();      
        clc;
        disp("METODO DE LA SECANTE");
        y=funcion 
        grid;
        ezplot(y);
        
        p0=aproximacion1
        p1=aproximacion2
        %Vuelve la funcion lineal
        f=inline(y,'x');
        
        TOL=tolerancia
        No=10
        
        %Muestra un cuadro con los datos a ingresados (1ra iteracion) y los respectivos datos arrojado en cada iteracion
        fprintf('\nInteracciones      p0          p1           p          f(p)\n');
        fprintf('      %d         %4.4f      %4.4f\n',1,p0,p1);
        %Definimos el valor de las variables evaluando los valores ingresados por pantalla en la funci�n
        i=2;
        q0 = f(p0);
        q1 = f(p1);
        %Entramos en un ciclo repetitivo, se cumplira siempre y cuando el indice sea menor o igual que el numero de iteraciones
        while (i<=No)
        p = p1-q1*(p1-p0)/(q1-q0);
        %Tabla para mostrar los resultados de cada iteraci�n
        fprintf('\n      %d         %4.4f      %4.4f        %4.4f       %4.4f\n' ,i,p0,p1,p,f(p));
        %Indica el valor de la raiz a la solucion aproximada de la ultima iteracion
        if abs(p-p1)<TOL
        %Imprimimos el resultado por pantalla:
        fprintf('\n * * * La Solucion Aproximada P es = %4.4f * * * \n',p);
        break
        end
        %Incrementa y sustituye los valores iniciales por los valores obtenido en la ultima iteracion
        i=i+1; %PASO 5

        p0=p1;
        q0=q1;
        p1=p;
        q1=f(p);
        end   
        
        if i>No
        fprintf('\n El metodo fracaso despues de %d iteraciones\n',No);
        end
        printf(' * * * Tiempo en que tarda en ejecutarse: %f segundos * * * \n', cputime-t);